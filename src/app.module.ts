import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProspectoModule } from './prospecto/prospecto.module';
import { MongooseModule} from '@nestjs/mongoose' 

@Module({
  imports: [ProspectoModule,
    MongooseModule.forRoot('mongodb://127.0.0.1/Prospectos')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
