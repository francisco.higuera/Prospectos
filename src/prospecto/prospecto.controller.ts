import { Body, Controller, Get, Post, Put } from '@nestjs/common';
import { response } from 'express';
import { json } from 'stream/consumers';
import ActualizarProspectoDto from './DTOs/ActualizarProspectoDTO';
import CrearProspectoDto from './DTOs/CrearProspectoDTO';
import ProspectoResponseDto from './DTOs/CrearProspectoResponseDTO';
import { ProspectoService } from './prospecto.service';

@Controller('prospecto')
export class ProspectoController {
    constructor(private service: ProspectoService){}

    @Get('/getAll')
    async getProspectos(){
        try {
            var response:ProspectoResponseDto = new ProspectoResponseDto();
            response.type = 0;
            response.data = await this.service.findProspectos();
            return response;
        } catch (exception) {
            return exception;
        }
        
    }

    @Post('CrearProspecto')
    async crearProspecto(@Body() crearProspecto: CrearProspectoDto){
        try{
           var response:ProspectoResponseDto = new ProspectoResponseDto();
            response.type = 0;
            response.data = await this.service.crearProspecto(crearProspecto);
            return  response;
        // return this.service.crearProspecto(crearProspecto);

        }catch(exception){
            return exception;
        }
    }

    @Put('ActualizarProspecto')
    async actualizarProspecto(@Body() prospecto:ActualizarProspectoDto){
        try{
            var response:ProspectoResponseDto = new ProspectoResponseDto();
            response.type = 0;
            response.data = await this.service.actualizarProspecto(prospecto.RFC,prospecto.status, prospecto.observaciones);

            return response;
        }catch(exception){
            return exception;
        }
        
    }
}