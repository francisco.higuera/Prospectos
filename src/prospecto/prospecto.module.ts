import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProspectoController } from './prospecto.controller';
import { ProspectoModelService } from './prospecto.model';
import { ProspectoService } from './prospecto.service';
import { ProspectoScheme } from './schema/prospecto.schema';


@Module({
  imports:[MongooseModule.forFeature([
    {name: 'prospectos', schema:ProspectoScheme }
])],
  controllers: [ProspectoController],
  providers: [ProspectoService,ProspectoModelService]
})
export class ProspectoModule {}
