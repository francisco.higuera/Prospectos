import { HttpException, Injectable, HttpStatus } from '@nestjs/common';
import CrearProspectoDto from './DTOs/CrearProspectoDTO';
import { ProspectoModelService } from './prospecto.model';

@Injectable()
export class ProspectoService {


    constructor(
        private readonly prospectosModelServices:ProspectoModelService
    ){}

    async findProspectos(){
        try{
            return await this.prospectosModelServices.find();
        }catch(exception){
            console.log(exception);
            throw new HttpException('Ha ocurrido un error',HttpStatus.NOT_FOUND);
        }
    }

    async crearProspecto(prospecto:CrearProspectoDto){
        try{
            return await this.prospectosModelServices.crearProspecto(prospecto);
        }catch(exception){
            console.log(exception);
            throw new HttpException('Ha ocurrido un error',HttpStatus.NOT_FOUND);       
        }
    }

    async actualizarProspecto(RFC:string,status:string, observaciones:string){
        try{
            return await this.prospectosModelServices.actualizarStatusProspecto(RFC,status,observaciones);
        }catch(exception){
            console.log(exception);
            return exception;
        }
    }
}
