import * as mongoose from 'mongoose';
import DocumentoDto from '../DTOs/DocumentoDTO';

export const ProspectoScheme = new mongoose.Schema({
    nombre:String,
    primerApellido:String,
    segundoApellido:String,
    calle:String,
    numero:Number,
    colonia:String,
    codigoPostal:Number,
    telefono:String,
    RFC: String,
    documentos:Array,
    status:String,
    observaciones:String,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
})

export interface Prospectos extends mongoose.Document{
    nombre:string,
    primerApellido:string,
    segundoApellido:string,
    calle:string,
    numero:number,
    colonia:string,
    codigoPostal:number,
    telefono:string,
    RFC: string,
    documentos:Array<DocumentoDto>,
    status: string,
    observaciones:string
}