import CrearProspectoDto from "./CrearProspectoDTO";

export default class ProspectoResponseDto{
    type:number
    message: string;
    data: Object;
}