import DocumentoDto from './DocumentoDTO';
export default class CrearProspectoDto{
    nombre:string;
    primerApellido:string;
    segundoApellido:string;
    calle:string;
    numero:number;
    colonia:string;
    codigoPostal:number;
    telefono:string;
    RFC: string;
    documentos:Array<DocumentoDto>;
    status:string;
    observaciones:String;
}
