import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {Model } from 'mongoose';
import CrearProspectoDto from './DTOs/CrearProspectoDTO';
import {Prospectos} from './schema/prospecto.schema'


@Injectable()
export class ProspectoModelService{
    constructor(@InjectModel('prospectos') private readonly prospectosModel:Model<Prospectos>){}

    async find(){
        return  await this.prospectosModel.find().exec();
    }

    async findByRFC(RFC:string){
        return this.prospectosModel.find().where('RFC').equals(RFC).exec();
    }

    async crearProspecto(prospecto:CrearProspectoDto){
        const buscarPorRFC = await this.findByRFC(prospecto.RFC);
        if(buscarPorRFC.length == 0){
            const crearProspecto = new this.prospectosModel(prospecto);
             await crearProspecto.save();
             return "El prospecto se ha dado de alta correctamente";
        }else{
            return "El prospecto ya esta dado de alta";
        }
    }
    async findByID(id:string){
        return this.prospectosModel.findOne().where('_id').equals(id).exec();
    }

    async actualizarStatusProspecto(RFC:string, status:string, observaciones:string){
        const prospecto = await this.findByRFC(RFC);
        
        prospecto[0].status = status;
        prospecto[0].observaciones = observaciones;
        prospecto[0].save();
        return "El prospecto se actualizo correctamente";
    }

}